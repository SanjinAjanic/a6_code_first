# Project Title
a6_code_first

# Descripton 
This is a school-assignment where we work whit code first to set up a new Database where we use restful api to interact whit the database and seting it up whit MVC design patterns

# Usages

# Tools
Install: 
Visual studio
Microsoft SQL Server Management Studio
Install Dependencies below on nuget package manager in Visual studio.

# Dependencies
AutoMapper.Extensions.Microsoft.DependencyInjection:
AutoMapper extensions for ASP.NET Core

Microsoft.EntityFrameworkCore.SqlServer:
Microsoft SQL Server database provider for Entity Framework Core.

Microsoft.EntityFrameworkCore.Tools:
Entity Framework Core Tools for the NuGet Package Manager Console in Visual Studio.
Enables these commonly used commands:
Add-Migration, Drop-Database, Get-DbContext, Get-Migration, Remove-Migration, Scaffold-DbContext,
Script-Migration, Update-Database.

Microsoft.VisualStudio.Web.CodeGeneration.Design:
Code Generation tool for ASP.NET Core. Contains the dotnet-aspnet-codegenerator command used for generating controllers and views.

Swashbuckle.AspNetCore:
Swagger tools for documenting APIs built on ASP.NET Core

# Setup
git clone git@gitlab.com:SanjinAjanic/a6_code_first.git

# Authors
Sanjin Ajanic 
Tim Jonasson






