﻿using a6_code_first.Models.DTOs.Movies;
using AutoMapper;
using System.Linq;
using WebAPI.Models;

namespace a6_code_first.Profiles
{
    /// <summary>
    /// Mapping of the MoviesProfile
    /// </summary>
    public class MoviesProfile : Profile
    {
        public MoviesProfile()
        {
            CreateMap<Movies, MoviesReadDTO>()
                .ForMember(mdto => mdto.Characters, opt => opt
                .MapFrom(m=>m.Characters.Select(c=>c.CharacterId).ToList()))
                .ReverseMap();

            CreateMap<Movies, MoviesCreateDTO>()
                .ReverseMap();

        }
    }
}
