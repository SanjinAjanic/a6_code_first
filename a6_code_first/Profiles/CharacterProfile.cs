﻿using a6_code_first.Models.DTOs.Character;
using a6_code_first.Models.DTOs.Movies;
using AutoMapper;
using System.Linq;
using WebAPI.Models;

namespace a6_code_first.Profiles
{
    /// <summary>
    /// Mapping of the CharacterProfile
    /// </summary>
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
             CreateMap<Character, CharacterReadDTO>()
            .ForMember(adto => adto.Movies, opt => opt
            .MapFrom(a => a.Movies.Select(m=>m.MoviesId).ToList()))
            .ReverseMap();
            // Character <->CharacterCreateDTO
             CreateMap<Character, CharacterCreateDTO>()
             .ReverseMap();
            // Character <->CharacterEditDTO
             CreateMap<Character, CharacterEditDTO>()
            .ReverseMap();
        }
    }
}
