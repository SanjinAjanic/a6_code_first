﻿using System.Collections.Generic;
using WebAPI.Models;

namespace a6_code_first.Models.DTOs.Character
{
    /// <summary>
    /// properties for CharacterEditDTO
    /// </summary>
    public class CharacterEditDTO
    {
        public int CharacterId { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public Gender Gender { get; set; }
        public string Picture { get; set; }
    }
}
