﻿using System.Collections.Generic;
using WebAPI.Models;

namespace a6_code_first.Models.DTOs.Character
{
    /// <summary>
    /// properties for CharacterCreateDTO
    /// </summary>
    public class CharacterCreateDTO
    {
        public string Name { get; set; }
        public string Alias { get; set; }
        public Gender Gender { get; set; }
        public string Picture { get; set; }

    }
}
