﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace a6_code_first.Models.DTOs.Movies
{
    /// <summary>
    /// properties for MoviesReadDTO
    /// </summary>
    public class MoviesReadDTO
    {
        public int MoviesId { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
        public string Franchise { get; set; }
        public List<int> Characters { get; set; } // Many -> One
    }
}
