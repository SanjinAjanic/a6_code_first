﻿using System.Collections.Generic;

namespace a6_code_first.Models.DTOs.Movies
{
    /// <summary>
    /// properties for MoviesCreateDTO
    /// </summary>
    public class MoviesCreateDTO
    {
        public string Title { get; set; }
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
    }
}
