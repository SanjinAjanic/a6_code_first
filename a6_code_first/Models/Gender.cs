﻿namespace WebAPI.Models
{
    /// <summary>
    /// Enum for the Gender
    /// </summary>
    public enum Gender
    {
        Woman,
        Man,
        Other
    }
}
