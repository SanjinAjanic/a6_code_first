﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebAPI.Models
{
    /// <summary>
    /// Model for Franchise
    /// </summary>
    [Table("Franchise")]
    public class Franchise
    {
        public int FranchiseId { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }

        public ICollection<Movies> Movies { get; set; } // Many -> one


    }
}
