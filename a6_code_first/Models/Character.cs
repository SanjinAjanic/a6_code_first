﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebAPI.Models
{
    /// <summary>
    /// Model of Character
    /// </summary>
    [Table("Character")]
    public class Character
    {
        [Key]
        public int CharacterId { get; set; }

        [MaxLength(50)]
        [Required]
        public string Name { get; set; }

        [MaxLength(50)]
        public string Alias { get; set; }

        [MaxLength(50)]
        public Gender Gender { get; set; }

        [MaxLength(500)]
        [Url]
        public string Picture { get; set; }

        public ICollection<Movies> Movies { get; set; } // Many -> many
    }
}
