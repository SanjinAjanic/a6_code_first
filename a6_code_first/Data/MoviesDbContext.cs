﻿using Microsoft.EntityFrameworkCore;
using WebAPI.Models;

namespace WebAPI.Data
{
    public class MoviesDbContext : DbContext
    {

        public MoviesDbContext(DbContextOptions options) : base(options)
        {

        }

        public MoviesDbContext()
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Seed();
        }
        /// <summary>
        /// DBset for all
        /// </summary>
        public DbSet<Character> Character { get; set; }
        public DbSet<Franchise> Franchise { get; set; }
        public DbSet<Movies> Movies { get; set; }
    }
}
