﻿using a6_code_first.Models.DTOs.Character;
using a6_code_first.Service.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPI.Data;
using WebAPI.Models;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace a6_code_first.Service
{
    public class CharacterRepo : IRepo
    {
        private readonly MoviesDbContext _dbContext;

        public CharacterRepo(MoviesDbContext moviesDbContext)
        {
            _dbContext = moviesDbContext;
        }
        /// <summary>
        /// Inserting a character
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public async void Add(Character character)
        {
            _dbContext.Character.Add(character);
            await _dbContext.SaveChangesAsync();
        }
        /// <summary>
        /// Deleting character
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task Delete( int id)
        {
            var character = await _dbContext.Character.FindAsync(id);
            _dbContext.Character.Remove(character);
            await _dbContext.SaveChangesAsync();
        }
        /// <summary>
        /// Gets all characters
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Character>> GetAll()
        {
            return await _dbContext.Character
                .ToListAsync();
        }
        
        /// <summary>
        /// Gets character byId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Character> GetById(int id)
        {
           return await _dbContext.Character.FindAsync(id);

        }
        /// <summary>
        /// Updating a character
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Task<IActionResult> Update(int id, CharacterEditDTO entity)
        {
            throw new System.NotImplementedException();
        }
       
    }
}
      
