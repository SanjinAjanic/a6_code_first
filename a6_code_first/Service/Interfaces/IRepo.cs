﻿using a6_code_first.Models.DTOs.Character;
using Microsoft.AspNetCore.Mvc;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPI.Models;

namespace a6_code_first.Service.Interfaces
{
    /// <summary>
    /// Generic CRUD
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepo
    {
       public Task <Character> GetById(int id);
       public Task <IEnumerable<Character>> GetAll();
       public void Add(Character entity);
       public Task Delete(int id);

    }
}
