﻿using a6_code_first.Models.DTOs.Movies;
using System.Collections;
using System.Collections.Generic;
using WebAPI.Models;

namespace a6_code_first.Service.Interfaces
{
    public interface IMoviesRepo
    {
        IEnumerable<MoviesReadDTO> GetAllMoviesInFranchise(int franchiseId);

    }
}
