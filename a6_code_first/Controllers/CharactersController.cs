﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using a6_code_first.Models.DTOs.Character;
using a6_code_first.Service;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Data;
using WebAPI.Models;

namespace a6_code_first.Controllers
{
    [Route("api/v1/characters")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {

        private readonly IMapper _mapper;
        private readonly MoviesDbContext _dbContext;

        public CharactersController(IMapper mapper,MoviesDbContext dbContext)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all characters
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _dbContext.Character.Include(m=>m.Movies).ToListAsync());
        }

        /// <summary>
        /// Get specific characters by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            var character = await _dbContext.Character.FindAsync(id); 

            if (character == null)
            {
                return NotFound();
            }
            return _mapper.Map<CharacterReadDTO>(character);
        }

        /// <summary>
        /// Update an character
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characterDto"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterEditDTO characterDto)
        {

                if (id != characterDto.CharacterId)
                {
                    return BadRequest();
                }
            Character domainCharacter = _mapper.Map<Character>(characterDto);
            _dbContext.Entry(domainCharacter).State = EntityState.Modified;

            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }

        /// <summary>
        /// Add a new character
        /// </summary>
        /// <param name="characterDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter(CharacterCreateDTO characterDto)
        {
            Character domainCharacter = _mapper.Map<Character>(characterDto);
            _dbContext.Character.Add(domainCharacter);

            return CreatedAtAction("GetCharacter",
                new { id = domainCharacter.CharacterId }, 
                _mapper.Map<CharacterReadDTO>(domainCharacter));
        }

        /// <summary>
        /// Delete an character
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            var character = await _dbContext.Character.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }
            _dbContext.Character.Remove(character);

            await _dbContext.SaveChangesAsync();

            return NoContent();
        }
        /// <summary>
        /// Gets all characters franchaises
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns></returns>
        [HttpGet("{franchiseId}/GetAllCharactersInFranchise")]
        public async Task<IEnumerable<CharacterReadDTO>> GetAllCharactersInFranchise(int franchiseId)
        {
            var franchise = await _dbContext.Franchise.FindAsync(franchiseId);
            List<CharacterReadDTO> characters = new();
            foreach (var movie in franchise.Movies)
            {
                foreach (var character in movie.Characters)
                {
                    CharacterReadDTO cdto = _mapper.Map<CharacterReadDTO>(character);
                    characters.Add(cdto);
                }
            }
            return characters;
        }
        /// <summary>
        /// Gets all the characters in movie
        /// </summary>
        /// <param name="movieId"></param>
        /// <returns></returns>
        [HttpGet("{movieId}/CharactersInMovie")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetAllCharactersInMovie(int movieId)
        {
            List<CharacterReadDTO> characters = new();

            Movies movie = _dbContext.Movies.Find(movieId);
            var results = _dbContext.Character
                .Include(m => m.Movies).ThenInclude(x => x.Characters);

            foreach (var item in results)
            {
                characters.Add(_mapper.Map<CharacterReadDTO>(item));
            }
            return Ok(characters);
        }

        /// <summary>
        /// true if character with id exist
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool CharacterExists(int id)
        {
            return _dbContext.Character.Any(e => e.CharacterId == id);
        }
    }
}
