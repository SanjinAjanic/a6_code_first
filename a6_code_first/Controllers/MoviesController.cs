﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using a6_code_first.Models.DTOs.Movies;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Data;
using WebAPI.Models;

namespace a6_code_first.Controllers
{
    [Route("api/v1/movies")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : ControllerBase
    {
        private readonly MoviesDbContext _context;
        private readonly IMapper _mapper;


        public MoviesController(MoviesDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all movies
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MoviesReadDTO>>> GetMovies()
        {
            return _mapper.Map<List<MoviesReadDTO>>(await _context.Movies.Include(m=>m.Characters).ToListAsync());
        }

        /// <summary>
        /// Get specific movies by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<Movies>> GetMovies(int id)
        {
            var movies = await _context.Movies.FindAsync(id);

            if (movies == null)
            {
                return NotFound();
            }

            return movies;
        }

        /// <summary>
        /// Update an movie
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchise"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovies(int id, Movies movies)
        {
            if (id != movies.MoviesId)
            {
                return BadRequest();
            }

            _context.Entry(movies).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MoviesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Add a new movie
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Movies>> PostMovies(MoviesCreateDTO movieDto)
        {
            Movies domainMovie = _mapper.Map<Movies>(movieDto);
            _context.Movies.Add(domainMovie);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovies",
                new { id = domainMovie.MoviesId },
                _mapper.Map<MoviesReadDTO>(domainMovie));
        }

        /// <summary>
        /// Delete an movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovies(int id)
        {
            var movies = await _context.Movies.FindAsync(id);
            if (movies == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movies);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        /// <summary>
        /// Update movie characters
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characters"></param>
        /// <returns></returns>

        [HttpPut("{id}/characters")]
        public async Task<IActionResult> UpdateMovieCharacters(int id, List<int> characters)
        {
            if (!MoviesExists(id))
            {
                return NotFound();
            }

            // May want to place this in a service - controller is getting bloated
            Movies movieToUpdateCharacters = await _context.Movies
                .Include(c => c.Characters)
                .Where(c => c.MoviesId == id)
                .FirstAsync();

            // Loop through characters, try and assign to movie
            // Trying to see if there is a nicer way of doing this, dont like the multiple calls
            List<Character> certs = new();
            foreach (int certId in characters)
            {
                Character cert = await _context.Character.FindAsync(certId);
                if (cert == null)
                    return BadRequest("Character doesnt exist!");
                certs.Add(cert);
            }
            movieToUpdateCharacters.Characters = certs;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }



        /// <summary>
        /// Movie exist
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool MoviesExists(int id)
        {
            return _context.Movies.Any(e => e.MoviesId == id);
        }
    }
}
